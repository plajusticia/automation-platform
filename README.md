# Introduction  
This repo contains the code needed to configure an Ansible Tower Installation getting it ready to deploy a Netbackup Environment composed by a netbackup Master/Media Server and a netbackup client. 
Deploying use cases of the Backup-cool initiative will also be configured on the Ansible Tower


# Requirements
In order to use this code, an Ansible Tower installation, version 3.4.1-1 must be up and runing.


Te following ste


# License

To be defined
# High Level Process Description

This code asumes that will be run locally on the Ansible Tower server.



# Variables
Before, review the following variables before runing the playbook



| Location | Name | Value | Notes |
| -------- | -------- | -------- |--------
| vars/main.yml     |  base_path     | api/v2     | common part of the uri to acces Tower API  |
| vars/main.yml | servers | List of the servers that compounds the lab environment with hostname and ip address| N/A |
| vars/main.yml | tower_hn |hostname or ip addres of the Tower server| default value: localhost|



# How To Use

